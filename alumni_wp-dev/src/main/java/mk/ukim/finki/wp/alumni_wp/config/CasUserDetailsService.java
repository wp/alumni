/*

package mk.ukim.finki.wp.alumni_wp.config;


import mk.ukim.finki.wp.alumni_wp.model.FinishedStudent;
import mk.ukim.finki.wp.alumni_wp.model.Professor;
import mk.ukim.finki.wp.alumni_wp.model.User;
import mk.ukim.finki.wp.alumni_wp.model.exceptions.InvalidUsernameException;
import mk.ukim.finki.wp.alumni_wp.model.exceptions.ProfessorNotFoundException;
import mk.ukim.finki.wp.alumni_wp.repository.FinishedStudentRepository;
import mk.ukim.finki.wp.alumni_wp.repository.ProfessorRepository;
import mk.ukim.finki.wp.alumni_wp.repository.UserRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Profile("cas")
@Service
public class CasUserDetailsService extends FacultyUserDetailsService implements AuthenticationUserDetailsService {


    public CasUserDetailsService(UserRepository userRepository,
                                 ProfessorRepository professorRepository,
                                 FinishedStudentRepository finishedStudentRepository,
                                 PasswordEncoder passwordEncoder) {
        super(userRepository, professorRepository,finishedStudentRepository, passwordEncoder);
    }

    @Override
    public UserDetails loadUserDetails(Authentication token) throws UsernameNotFoundException {
        String username = (String) token.getPrincipal();

        if (userRepository.existsById(username)) {
            User user = userRepository.findById(username).orElseThrow(() -> new UsernameNotFoundException("User not found"));

            if (user.getRole().isProfessor()) {
                Professor professor = professorRepository.findById(username).orElseThrow(ProfessorNotFoundException::new);
                return new FacultyUserDetails(user, professor, passwordEncoder.encode(systemAuthenticationPassword));
            } else if (user.getRole().isStudent()) {
                FinishedStudent finishedStudent = finishedStudentRepository.findByIndexNumber(username)
                        .orElseThrow(() -> new UsernameNotFoundException("Finished student not found"));
                return new FacultyUserDetails(user, finishedStudent, passwordEncoder.encode(systemAuthenticationPassword));
            } else {
                throw new UsernameNotFoundException("User role not recognized");
            }
        }

        throw new UsernameNotFoundException("User not found");
    }

}
*/
