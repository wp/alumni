package mk.ukim.finki.wp.alumni_wp.repository;


import mk.ukim.finki.wp.alumni_wp.model.Student;

public interface StudentRepository extends JpaSpecificationRepository<Student, String> {
}
