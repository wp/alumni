package mk.ukim.finki.wp.alumni_wp.model;

public enum AppRole {
    PROFESSOR, ADMIN, GUEST, FINISHED_STUDENT, DEV_TEAM; ;


    public String roleName() {
        return "ROLE_" + this.name();
    }
}
