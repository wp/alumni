package mk.ukim.finki.wp.alumni_wp.model.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class FinishedStudentNotFoundException extends RuntimeException {
}
