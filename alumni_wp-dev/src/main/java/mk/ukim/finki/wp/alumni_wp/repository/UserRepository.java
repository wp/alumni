package mk.ukim.finki.wp.alumni_wp.repository;


import mk.ukim.finki.wp.alumni_wp.model.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaSpecificationRepository<User, String> {

}
