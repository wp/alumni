package mk.ukim.finki.wp.alumni_wp.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.cas.authentication.CasAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.logout.LogoutFilter;

@Profile(("cas"))
@Configuration
@EnableWebSecurity
public class CasSecurityConfig extends AuthConfig {

    @Autowired
    private CasAuthenticationProvider casAuthenticationProvider;

    @Autowired
    private CasAuthenticationConfigurer casConfigurer;

    @Bean
    @Order(100)
    public SecurityFilterChain casSecurityFilterChain(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests(authorize -> authorize
                        .requestMatchers("/alumni/add-form", "/alumni/edit/**", "/alumni/delete/**").authenticated() // Require authentication
                        .requestMatchers("/alumni", "/alumni/fakti").permitAll() // Allow access without authentication
                        .anyRequest().permitAll()
                )
                .logout(logout -> logout
                        .logoutUrl("/logout")
                        .logoutSuccessUrl("/")
                        .invalidateHttpSession(true)
                        .deleteCookies("JSESSIONID")
                )
                .exceptionHandling(handler -> handler
                        .authenticationEntryPoint(casConfigurer.casAuthenticationEntryPoint())
                )
                .addFilter(casConfigurer.casAuthenticationFilter(http.getSharedObject(AuthenticationManager.class)))
                .addFilterBefore(casConfigurer.requestCasGlobalLogoutFilter(), LogoutFilter.class)
                .authenticationProvider(casAuthenticationProvider);


        return http.build();
    }

}