package mk.ukim.finki.wp.alumni_wp.repository;

import mk.ukim.finki.wp.alumni_wp.model.FinishedStudent;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import java.util.Optional;

@Repository
public interface FinishedStudentRepository extends JpaRepository<FinishedStudent, Integer> {


    Page<FinishedStudent> findByNameContainingIgnoreCase(String name, Pageable pageable);
    Page<FinishedStudent> findByIndexNumber(String indexNumber, Pageable pageable);

    Optional<FinishedStudent> findFinishedStudentByIndexNumber(String indexNumber);

    void deleteFinishedStudentByIndexNumber(String indexNumber);

    Page<FinishedStudent> findFinishedStudentsByIndexNumberContaining(String indexNumber, Pageable pageable);
}
