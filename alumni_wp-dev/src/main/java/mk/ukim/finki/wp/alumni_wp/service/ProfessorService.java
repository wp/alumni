package mk.ukim.finki.wp.alumni_wp.service;

import mk.ukim.finki.wp.alumni_wp.model.Professor;
import mk.ukim.finki.wp.alumni_wp.model.exceptions.ProfessorNotFoundException;


import java.util.List;

public interface ProfessorService {


    List<Professor> getAllProfessors();

    Professor getProfessorById(String id) throws ProfessorNotFoundException;
}
