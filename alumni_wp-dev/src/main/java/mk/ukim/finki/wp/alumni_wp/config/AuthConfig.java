
package mk.ukim.finki.wp.alumni_wp.config;

import mk.ukim.finki.wp.alumni_wp.model.AppRole;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.LogoutConfigurer;
import org.springframework.security.web.access.expression.WebExpressionAuthorizationManager;

@Configuration
@EnableWebSecurity
public class AuthConfig {

    public HttpSecurity authorize(HttpSecurity http) throws Exception {
        return http
                .csrf(csrf -> csrf.ignoringRequestMatchers("/api/**"))
                .authorizeHttpRequests(requests -> requests
                        // Only ADMIN can add, edit, and delete alumni
                        .requestMatchers("/alumni/add-form", "/alumni/edit/**", "/alumni/delete/**").hasRole("ADMIN")
                        // USERS with specific roles can access alumni pages
                        .requestMatchers("/alumni", "/alumni/**")
                        .hasAnyRole("USER", "ADMIN", "PROFESSOR", "DEV_TEAM", "FINISHED_STUDENT")
                        // Permit all other requests
                        .anyRequest().permitAll()
                )
                .formLogin(form -> form
                        .loginPage("/login")
                        .defaultSuccessUrl("/alumni", true)
                        .permitAll()
                )
                .logout(LogoutConfigurer::permitAll);
    }
}
