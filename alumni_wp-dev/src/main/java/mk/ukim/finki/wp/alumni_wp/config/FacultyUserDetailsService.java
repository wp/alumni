/*

package mk.ukim.finki.wp.alumni_wp.config;


import mk.ukim.finki.wp.alumni_wp.model.FinishedStudent;
import mk.ukim.finki.wp.alumni_wp.model.Professor;
import mk.ukim.finki.wp.alumni_wp.model.User;
import mk.ukim.finki.wp.alumni_wp.model.exceptions.InvalidUsernameException;
import mk.ukim.finki.wp.alumni_wp.model.exceptions.ProfessorNotFoundException;
import mk.ukim.finki.wp.alumni_wp.repository.FinishedStudentRepository;
import mk.ukim.finki.wp.alumni_wp.repository.ProfessorRepository;
import mk.ukim.finki.wp.alumni_wp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class FacultyUserDetailsService implements UserDetailsService {

   // @Value("${system.authentication.password}")
    @Value("${system.authentication.password}")
    String systemAuthenticationPassword;

    final UserRepository userRepository;

    final ProfessorRepository professorRepository;

    final PasswordEncoder passwordEncoder;
    final FinishedStudentRepository finishedStudentRepository;

    public FacultyUserDetailsService(UserRepository userRepository, ProfessorRepository professorRepository,  FinishedStudentRepository finishedStudentRepository,PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.professorRepository = professorRepository;
        this.passwordEncoder = passwordEncoder;
        this.finishedStudentRepository = finishedStudentRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findById(username).orElseThrow(InvalidUsernameException::new);
        if (user.getRole().isProfessor()) {
            Professor professor = professorRepository.findById(username).orElseThrow(ProfessorNotFoundException::new);
            return new FacultyUserDetails(user, professor, passwordEncoder.encode(systemAuthenticationPassword));
        }
        else if (user.getRole().isStudent()) {

            FinishedStudent finishedStudent = finishedStudentRepository.findByIndexNumber(username)
                    .orElseThrow(() -> new UsernameNotFoundException("Finished student not found"));

            return new FacultyUserDetails(user,finishedStudent,passwordEncoder.encode(systemAuthenticationPassword));
        }

        else {
            return new FacultyUserDetails(user, passwordEncoder.encode(systemAuthenticationPassword));
        }
    }
}

*/
