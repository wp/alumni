package mk.ukim.finki.wp.alumni_wp.service;

import com.opencsv.exceptions.CsvValidationException;
import jakarta.annotation.PostConstruct;
import mk.ukim.finki.wp.alumni_wp.model.FinishedStudent;
import mk.ukim.finki.wp.alumni_wp.repository.FinishedStudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.opencsv.CSVReader;

import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
public class CsvImportService {
    @Autowired
    private FinishedStudentRepository finishedStudentRepository;

    @PostConstruct
    public void importCsvData() throws IOException {
        String csvFile = "C:/Users/Administrator/Downloads/filtered_students_copy.csv";
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm");

        try (CSVReader reader = new CSVReader(new FileReader(csvFile))) {
            List<FinishedStudent> students = new ArrayList<>();
            String[] line;
            reader.readNext();
            while ((line = reader.readNext()) != null) {
                FinishedStudent student = new FinishedStudent();
                student.setThesisTitle(line[0]);
                student.setIndexNumber(line[1]);
                student.setName(line[2]);
                student.setMentor(line[3]);
                student.setDate(LocalDate.parse(line[4], dateFormatter));
                student.setTime(LocalTime.parse(line[5], timeFormatter));
                students.add(student);
            }
            finishedStudentRepository.saveAll(students);
        } catch (CsvValidationException e) {
            throw new RuntimeException(e);
        }
    }
}
