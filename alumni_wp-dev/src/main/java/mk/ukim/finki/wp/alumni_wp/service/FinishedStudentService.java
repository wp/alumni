package mk.ukim.finki.wp.alumni_wp.service;


import mk.ukim.finki.wp.alumni_wp.model.FinishedStudent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.time.LocalTime;

import java.util.Optional;

public interface FinishedStudentService {
    Page<FinishedStudent> listFinishedStudents(Pageable pageable);

    FinishedStudent findFinishedStudentByIndexNumber(String indexNumber);
    Optional<FinishedStudent> save(String thesisTitle, String indexNumber,
                                   String name, String mentor, LocalDate date, LocalTime time);

    Optional<FinishedStudent> edit(String thesisTitle, String indexNumber,
                                   String name, String mentor,LocalDate date, LocalTime time);

    void deleteFinishedStudentByIndexNumber(String indexNumber);
    Page<FinishedStudent> findByNameContainingIgnoreCase(String name, Pageable pageable);

    Page<FinishedStudent> findFinishedStudentsByIndexNumberContaining(String indexNumber, Pageable pageable);
}
