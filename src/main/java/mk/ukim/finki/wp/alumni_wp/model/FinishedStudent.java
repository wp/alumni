package mk.ukim.finki.wp.alumni_wp.model;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalTime;


@Getter
@Setter
@ToString
@Entity
public class FinishedStudent {

    @Id
    private String indexNumber;
    private String thesisTitle;
    private String name;
    private String mentor;

    @Column(columnDefinition = "DATE")
    private LocalDate date;
    private LocalTime time;

    public FinishedStudent(String indexNumber, String thesisTitle, String name, String mentor, LocalDate date, LocalTime time) {
        this.thesisTitle = thesisTitle;
        this.indexNumber = indexNumber;
        this.name = name;
        this.mentor = mentor;
        this.date = date;
        this.time = time;
    }

    public FinishedStudent() {
    }

}
