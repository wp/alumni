package mk.ukim.finki.wp.alumni_wp.service.impl;

import mk.ukim.finki.wp.alumni_wp.model.FinishedStudent;
import mk.ukim.finki.wp.alumni_wp.model.exceptions.FinishedStudentNotFoundException;
import mk.ukim.finki.wp.alumni_wp.repository.FinishedStudentRepository;
import mk.ukim.finki.wp.alumni_wp.service.FinishedStudentService;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalTime;

import java.util.Optional;

@Service
public class FinishedStudentServiceImpl implements FinishedStudentService {


    private final FinishedStudentRepository finishedStudentRepository;

    public FinishedStudentServiceImpl(FinishedStudentRepository finishedStudentRepository) {
        this.finishedStudentRepository = finishedStudentRepository;
    }


    public Page<FinishedStudent> listFinishedStudents(Pageable pageable){
        return finishedStudentRepository.findAll(pageable);
    }


    public FinishedStudent findFinishedStudentByIndexNumber(String indexNumber) {
        return finishedStudentRepository.findFinishedStudentByIndexNumber(indexNumber).
                orElseThrow(FinishedStudentNotFoundException::new);
    }

    @Override
    public Optional<FinishedStudent> save(String thesisTitle, String indexNumber,
                                          String name, String mentor, LocalDate date, LocalTime time) {
        return Optional.of(this.finishedStudentRepository.
                save(new FinishedStudent(thesisTitle, indexNumber, name, mentor,date, time)));
    }


    public Optional<FinishedStudent> edit(String thesisTitle, String indexNumber, String name, String mentor, LocalDate date, LocalTime time) {
       FinishedStudent finishedStudent=finishedStudentRepository.findFinishedStudentByIndexNumber(indexNumber).orElseThrow(FinishedStudentNotFoundException::new);

       finishedStudent.setThesisTitle(thesisTitle);
       finishedStudent.setIndexNumber(indexNumber);
       finishedStudent.setName(name);
       finishedStudent.setMentor(mentor);
       finishedStudent.setDate(date);
       finishedStudent.setTime(time);

       finishedStudentRepository.save(finishedStudent);
        return Optional.of(finishedStudent);

    }


    @Transactional
    public void deleteFinishedStudentByIndexNumber(String indexNumber) {
        this.finishedStudentRepository.deleteFinishedStudentByIndexNumber(indexNumber);
    }


    @Override
    public Page<FinishedStudent> findByNameContainingIgnoreCase(String name, Pageable pageable) {
        return finishedStudentRepository.findByNameContainingIgnoreCase(name, pageable);
    }


    public Page<FinishedStudent> findFinishedStudentsByIndexNumberContaining(String indexNumber, Pageable pageable) {
        return finishedStudentRepository.findFinishedStudentsByIndexNumberContaining(indexNumber, pageable);
    }




}
