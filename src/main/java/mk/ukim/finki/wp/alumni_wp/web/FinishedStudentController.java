package mk.ukim.finki.wp.alumni_wp.web;

import mk.ukim.finki.wp.alumni_wp.model.FinishedStudent;
import mk.ukim.finki.wp.alumni_wp.service.FinishedStudentService;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalTime;


@Controller
@RequestMapping("/alumni")
public class FinishedStudentController {

    private final FinishedStudentService finishedStudentService;

    public FinishedStudentController(FinishedStudentService finishedStudentService) {
        this.finishedStudentService = finishedStudentService;
    }

    @GetMapping
    public String getFinishedStudentsPageAndSearch(Model model,
                                                   @RequestParam(name = "page", defaultValue = "0") int page,
                                                   @RequestParam(name = "sortDir", defaultValue = "asc") String sortDir,
                                                   @RequestParam(required = false) String name,
                                                   @RequestParam(required = false) String indexNumber) {
        Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by("date").ascending() :
                Sort.by("date").descending();
        Pageable pageable = PageRequest.of(page, 10, sort);
        Page<FinishedStudent> results;

        if (name != null && !name.isEmpty()) {
            results = finishedStudentService.findByNameContainingIgnoreCase(name, pageable);
        } else if (indexNumber != null && !indexNumber.isEmpty()) {
            results = finishedStudentService.findFinishedStudentsByIndexNumberContaining(indexNumber, pageable);
        } else {
            results = finishedStudentService.listFinishedStudents(pageable);
        }

        model.addAttribute("finishedStudentPage", results);
        model.addAttribute("currentPage", page);
        model.addAttribute("sortDir", sortDir);
        model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");
        model.addAttribute("name", name);
        model.addAttribute("indexNumber", indexNumber);
        return "listFinishedStudents";
    }


    @GetMapping("/add-form")
    @PreAuthorize("hasRole('DEV_TEAM') or hasRole('PROFESSOR') or hasRole('FINISHED_STUDENT')")
    public String getAddFinishedStudent(){

        return "add-student";
    }


    @PostMapping("/add")
    @PreAuthorize("hasRole('DEV_TEAM') or hasRole('PROFESSOR') or hasRole('FINISHED_STUDENT')")
    public String saveFinishedStudent(@RequestParam String thesisTitle,
                           @RequestParam String indexNumber,
                           @RequestParam String name,
                           @RequestParam String mentor,
                           @RequestParam  LocalDate date,
                           @RequestParam LocalTime time){

        this.finishedStudentService.save(thesisTitle, indexNumber, name, mentor, date, time);
        return "redirect:/alumni";
    }

    @GetMapping("/edit/{studentId}")
    @PreAuthorize("hasRole('DEV_TEAM') or hasRole('PROFESSOR') or hasRole('FINISHED_STUDENT')")
    public String editFinishedStudentGet(@PathVariable String studentId, Model model){
        if(studentId==null) {
            return "redirect:/alumni?error=StudentNotFound";
        }

        FinishedStudent finishedStudent=finishedStudentService.findFinishedStudentByIndexNumber(studentId);

        model.addAttribute("finishedStudent",finishedStudent);

        return "edit-student";
    }

    @PostMapping("/delete/{id}")
    @PreAuthorize("hasRole('DEV_TEAM') or hasRole('PROFESSOR')")
    public String deleteFinishedStudent(@PathVariable String id){
        this.finishedStudentService.deleteFinishedStudentByIndexNumber(id);
        return "redirect:/alumni";
    }


    @PostMapping("/edit/{studentId}")
    @PreAuthorize("hasRole('DEV_TEAM') or hasRole('PROFESSOR') or hasRole('FINISHED_STUDENT')")
    public String editFinishedStudentPost(@PathVariable("studentId") String  indexNumber,
                                          @RequestParam String thesisTitle,
                                          @RequestParam String name,
                                          @RequestParam String mentor,
                                          @RequestParam LocalDate date,
                                          @RequestParam  LocalTime time) {

        this.finishedStudentService.edit(thesisTitle, indexNumber, name, mentor, date, time);
        return "redirect:/alumni";
    }

    @GetMapping("/fakti")
    public String fakti() {
        // This returns the "fakti.html" template
        return "fakti";
    }

}
