package mk.ukim.finki.wp.alumni_wp.repository;


import mk.ukim.finki.wp.alumni_wp.model.Professor;

public interface ProfessorRepository extends JpaSpecificationRepository<Professor, String> {

}
