package mk.ukim.finki.wp.alumni_wp.service.impl;


import lombok.AllArgsConstructor;
import mk.ukim.finki.wp.alumni_wp.model.Professor;
import mk.ukim.finki.wp.alumni_wp.model.exceptions.ProfessorNotFoundException;
import mk.ukim.finki.wp.alumni_wp.repository.ProfessorRepository;
import mk.ukim.finki.wp.alumni_wp.service.ProfessorService;

import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ProfessorServiceImpl implements ProfessorService {
    private final ProfessorRepository professorRepository;

    @Override
    public List<Professor> getAllProfessors() {
        return professorRepository.findAll(Sort.by("email"));
    }

    @Override
    public Professor getProfessorById(String id) throws ProfessorNotFoundException {
        return professorRepository.findById(id)
                .orElseThrow(ProfessorNotFoundException::new);
    }

}
