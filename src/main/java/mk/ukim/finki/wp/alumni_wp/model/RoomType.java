package mk.ukim.finki.wp.alumni_wp.model;

public enum RoomType {

    CLASSROOM, LAB, MEETING_ROOM, OFFICE, VIRTUAL
}
