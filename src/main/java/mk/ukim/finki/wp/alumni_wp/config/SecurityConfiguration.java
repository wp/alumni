
package mk.ukim.finki.wp.alumni_wp.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@Profile(("!cas"))
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .csrf(csrf -> csrf.disable())
                .authorizeHttpRequests(authorize -> authorize
                        .requestMatchers("/alumni", "/alumni/fakti").permitAll()
                        .requestMatchers("/alumni/add-form", "/alumni/edit/**", "/alumni/delete/**").hasAnyRole("USER", "ADMIN", "PROFESSOR", "DEV_TEAM", "FINISHED_STUDENT")
                        .anyRequest().permitAll()
                )
                .formLogin(form -> form
                        .loginPage("/login")
                        .defaultSuccessUrl("/alumni", true)
                        .permitAll()
                )
                .logout(logout -> logout
                        .logoutUrl("/logout")
                        .logoutSuccessUrl("/alumni")
                        .invalidateHttpSession(true)
                        .deleteCookies("JSESSIONID")
                        .permitAll()
                );

        return http.build();
    }

    @Bean
    public UserDetailsService userDetailsService() {
        return new InMemoryUserDetailsManager(
                org.springframework.security.core.userdetails.User.withUsername("212004")
                        .password(passwordEncoder().encode("aleksandra24"))
                        .roles("DEV_TEAM")
                        .build(),
                org.springframework.security.core.userdetails.User.withUsername("212012")
                        .password(passwordEncoder().encode("bojan24"))
                        .roles("DEV_TEAM")
                        .build(),
                org.springframework.security.core.userdetails.User.withUsername("213006")
                        .password(passwordEncoder().encode("mihaela24"))
                        .roles("DEV_TEAM")
                        .build(),
                org.springframework.security.core.userdetails.User.withUsername("213026")
                        .password(passwordEncoder().encode("matea24"))
                        .roles("DEV_TEAM")
                        .build()
        );
    }

}